# coding: utf-8
import numpy as np


def gate(xa, wa, b):
    x = np.array(xa)
    w = np.array(wa)
    tmp = np.sum(w * x) + b
    if tmp <= 0:
        return 0
    else:
        return 1


def AND(x1, x2):
    return gate([x1, x2], [0.5, 0.5], -0.7)


def NAND(x1, x2):
    return gate([x1, x2], [-0.5, -0.5], 0.7)


def OR(x1, x2):
    return gate([x1, x2], [0.5, 0.5], -0.2)


def XOR(x1, x2):
    s1 = NAND(x1, x2)
    s2 = OR(x1, x2)
    y = AND(s1, s2)
    return y


if __name__ == '__main__':
    array = [(0, 0), (1, 0), (0, 1), (1, 1)]
    print("params\t\tAND\t\tNAND\tOR\t\tXOR")
    for xs in array:
        print(str(xs)+"\t\t"+str(AND(xs[0], xs[1]))+"\t\t"+str(NAND(xs[0], xs[1]))+"\t\t"+str(OR(xs[0], xs[1]))+"\t\t"+str(XOR(xs[0], xs[1])))
